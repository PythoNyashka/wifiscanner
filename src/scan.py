
#                                        #
# This code was written by PythonNyashka #
#                                        #

import subprocess
from time import *
import json
from serial import Serial
ser = Serial("COM5", 2000000)
sleep(4)

def write_json(new_cmd_dict,file_name):
    try:
        data = json.load(open(file_name,'r', encoding="cp1251"))
    except:
        data = []
    data.append(new_cmd_dict)

    with open(file_name, 'w', encoding="cp1251") as file:
        json.dump(data, file, indent=2, ensure_ascii=False)

def write_network():
    ser.write(1)
    sleep(1)
    geolocation = str(ser.readline(ser.inWaiting()).decode("utf-8")[0: -2])
    latitude, longitude = geolocation.split(':')[0], geolocation.split(':')[1]
    ssid = ssids[i].split(":")[1].replace(' ', '')
    dict = {"ssid": ssid, "type": types[i], "authentication": authentications[i],
            "encryption": encryptions[i], "latitude": latitude, "longitude": longitude, }
    print(f"SSID: {ssid} latitude: {latitude} longitude: {longitude} ")
    write_json(dict, "nets.json")
        
print("while True starting now...")
while True:
    results = subprocess.check_output(["netsh", "wlan", "show", "network"])
    results = results.decode('cp866', "replace")
    info_mas = results.split("\n")

    ssids = []
    types = []
    authentications = []
    encryptions = []

    for i in info_mas:
        if "SSID" in i:
            ssids.append(i.replace("\r", ''))
    for i in info_mas:
        if "Тип сети:" in i:
            type = i.replace(' ', '').split(':')[1]
            types.append(type.replace("\r", ''))
    for i in info_mas:
        if "Проверка подлинности:" in i:
            authentication = i.replace(' ', '').split(':')[1]
            authentications.append(authentication.replace("\r", ''))
    for i in info_mas:
        if "Шифрование:" in i:
            encryption = i.replace(' ', '').split(':')[1]
            encryptions.append(encryption.replace("\r", ''))

    try:
        data = json.load(open("nets.json",'r', encoding="cp1251"))
    except:
        data = []
    ssids_json = []
    if data:
        for i in range(len(data)):
            ssids_json.append(data[i]["ssid"])

        for i in range(len(ssids)):
            if ssids[i].split(":")[1].replace(' ', '') in ssids_json:
                pass
            else:
                write_network()
    else:
        for i in range(len(ssids)):
            write_network()
    sleep(5)



