
#                                        #
# This code was written by PythonNyashka #
#                                        #

import json
import csv

try:
    data = json.load(open("nets.json", 'r', encoding="cp1251"))
except:
    data = []

with open('wifi_map.csv', mode='w') as csv_file:
    fieldnames = ['latitude', 'longitude', 'ssid']
    writer = csv.DictWriter(csv_file, fieldnames=fieldnames)

    writer.writeheader()
    for i in range(len(data)):
        writer.writerow({'latitude': data[i]['latitude'], 'longitude': data[i]['longitude'], 'ssid': data[i]['ssid']})
