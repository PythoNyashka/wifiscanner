
#                                        #
# This code was written by PythonNyashka #
#                                        #

import json
from time import *
from collections import Counter

authentications = []
stat = []
percent_stat = []
allcount = 0

def read_json(file_name):
    try:
        data = json.load(open(file_name,'r', encoding="cp1251"))
    except:
        data = []
    return data

data = read_json("nets.json")

if data:
    for net in data:
        authentications.append(net['authentication'])
        allcount += 1
    for key, value in Counter(authentications).items():
        stat.append({key: value})
    for auth in stat:
        for auth, count in auth.items():
            percent = count*100/allcount
            percent_stat.append({auth: round(percent)})
    with open("percent_stat.json", 'w', encoding="cp1251") as file:
        json.dump(percent_stat, file, indent=2, ensure_ascii=False)
else:
    print("Database is empty")
    sleep(5)
    exit(0)
